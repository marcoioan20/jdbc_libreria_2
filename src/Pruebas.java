import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.util.ArrayList;
import java.util.List;
import autores.Autor;
import autores.AutorController;
import escrito_por.Escrito_por;
import escrito_por.Escrito_porController;
import libros.Libros;
import libros.LibrosController;

public class Pruebas {

	public static void main(String[] args) {

		String cadena_conexion = "jdbc:mysql://localhost:3306/libreria?useUnicode=true&serverTimezone=UTC";
		String usuario = "root";
		String contrasena = "1234";

		Connection conn;
		try {
			conn = DriverManager.getConnection(cadena_conexion, usuario, contrasena);

			AutorController ac = new AutorController(conn);
			LibrosController lb = new LibrosController(conn);
			Escrito_porController ep = new Escrito_porController(conn);

			ArrayList autores = ac.selectAllAutores();
			System.out.println("Los autores de la tabla Autores son: ");
			for (int i = 0; i < autores.size(); i++) {
				System.out.println(autores.get(i).toString());
			}
			
			System.out.println("");
			System.out.println("Insertar, Borar y Actualizar \n");
			// Insertar el autor
			
			Autor autor = new Autor(10, "Y42356Q", "Miguelito", "Perez", 44);
			
			
			if (ac.insertAutor(autor) == true) {
				System.out.println("Autor insertado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido insertar el autor.");
			}
			
			
			int idBorar = 10; // el valor de la clave primaria de autor para eliminar
			
			// Borando el autor
			if (ac.deleteAutor(idBorar) == true) {
				System.out.println("Autor borado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido borar el autor.");
			}
			
			// Actualizar los datos de un autor, La clave primaria es primera que en este ejemplo es 10
			if (ac.updateAutor(10, "A42241Z", "Mulegrito", "Zukeberg", 44) == true) {
				System.out.println("Autor actualizado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido actualizar el autor.");
			}

			System.out.println("");
			//
			//
			//
			//Tabla de los libros
			ArrayList<Libros> libros = lb.selectAllLibros();

			System.out.println("Los libros de la tabla Libros son: ");
			for (int i = 0; i < libros.size(); i++) {
				System.out.println(libros.get(i).toString());
			}
			System.out.println("");
			System.out.println("Insertar, Borar y Actualizar \n");
			Libros libro = new Libros(5, "Programame", 2021, 15, "GidLab");
			// Insertar un libro
			if (lb.insertLibros(libro) == true) {
				System.out.println("Libro insertado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido insertar el libro.");
			}
			
			
			
			// Actualizar los datos de un libro, La clave primaria es primera que en este ejemplo es 5
			if (lb.updateLibros(5, "Broz", 2015, 54, "Gio") == true) {
				System.out.println("Libro actualizado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido actualizar el Libro.");
			}

			int idBorarLibros = 5; // el valor de la clave primaria de libros para eliminar
			// Borando el libro
			if (lb.deleteLibros(idBorarLibros) == true) {
				System.out.println("Libro borado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido borar el Libro.");
			}
			System.out.println("");
			
			//
			//
			//
			//
			//Tabla de escrito_por
			System.out.println("Los valores de la tabla Escrito_por son: ");
			
			ArrayList<Escrito_por> escrito = ep.selectAllEscrito_por();

			for (int i = 0; i < escrito.size(); i++) {
				System.out.println(escrito.get(i).toString());
			}
			
			System.out.println("");
			System.out.println("Insertar, Borar y Actualizar \n");
			Escrito_por escritoP = new Escrito_por(5, 5);
			
			if (ep.insertLibros(escritoP) == true) {
				System.out.println("Insertado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido insertar.");
			}
			
			
			
			// Actualizar los datos de la tabla, La clave primaria es primera que en este ejemplo es 5
			if (ep.updateLibros(5, 4) == true) {
				System.out.println("Tabla actualizado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido actualizar la Tabla.");
			}
			int idBorarEP = 5; // el valor de la clave primaria de libros para eliminar
			// Borando una linea de la talba
			if (ep.deleteLibros(idBorarEP) == true) {
				System.out.println("Borado corectamente");
			} else {
				System.out.println("Ha abido un error, no se ha podido borar.");
			}
			
			//New content coming soon....

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		

	}
}

                                                                              //Old Shit


//String consulta = "select * "+ " from autores;";
		// PreparedStatement ps = conn.prepareStatement(consulta,
		// ResultSet.TYPE_SCROLL_SENSITIVE, ResultSet.CONCUR_READ_ONLY);
		// System.out.println(ps);

		// insertamos un autor
		/*
		 * Autor autor1 = new Autor(4, "9876543E", "Gloria", "Fuertes", 83); if
		 * (ac.insertAutor(autor1))
		 * System.out.println("El autor se ha insertado correctamente"); else
		 * System.out.println("El autor NO se ha insertado"); }catch(Exception e) {
		 * e.printStackTrace(); }
		 */

/*
 * System.out.println("Id |" + "|    DNI   |" + "| Edad |" + "|   Nombre  |" +
 * "|  Apellido  |"); ResultSet rs = ac.selectAllAutores(); ResultSet rs1 =
 * lb.selectAllLibros(); ResultSet rs2 = ep.selectAllEscrito_por(); while
 * (rs.next()) { int id = rs.getInt("nAutores"); String dni =
 * rs.getString("dni"); String nombre = rs.getString("nombre"); String apellido
 * = rs.getString("apellidos"); int edad = rs.getInt("edad"); // Autor a1 = new
 * Autor(); System.out.println(id + " -  " + dni + "     " + edad + "        " +
 * nombre + "       " + apellido); } System.out.println("\n");
 * System.out.println("Id |" + "| Titulo   |" + "| A�o publicacion |" +
 * "| Nr. Paginas |" + "| Editorial |" ); while (rs1.next()) { int id1 =
 * rs1.getInt("nLibros"); String titulo = rs1.getString("nTitulo"); int annoPub
 * = rs1.getInt("nAnnoPublicacion"); int numPag = rs1.getInt("nNumPaginas");
 * String editorial = rs1.getString("nEditorial"); System.out.println(id1 +
 * " - " + titulo + "         " + annoPub + "            " + numPag + "        "
 * + editorial); }
 * 
 * System.out.println("\n"); System.out.println("Id Autor |" + "| Id Libros" );
 * while (rs2.next()) { int id2 = rs2.getInt("nAutores"); int id3 =
 * rs2.getInt("nLibros"); System.out.println(id2 + "               " + id3);
 */