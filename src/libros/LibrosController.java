package libros;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import autores.Autor;

public class LibrosController implements LibrosDAO {
	
	private Connection con;

	public LibrosController(Connection con) {
		super();
		this.con = con;
	}

	@Override
	public ArrayList<Libros> selectAllLibros() {

		ArrayList<Libros> libros = new ArrayList<Libros>();

		try {
			String query = "Select * from libros";
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(query);

			// rs.beforeFirst();

			while (rs.next()) {
				int nLibros = rs.getInt("nLibros");
				String nTitulo = rs.getString("nTitulo");
				int nAnnoPublicacion = rs.getInt("nAnnoPublicacion");
				int nNumPaginas = rs.getInt("nNumPaginas");
				String nEditorial = rs.getString("nEditorial");

				Libros libro = new Libros(nLibros, nTitulo, nAnnoPublicacion, nNumPaginas, nEditorial);
				libros.add(libro);

			}
			stm.close();
			rs.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return libros;

	}

	@Override
	public boolean insertLibros(Libros libros) {
		String sql = "insert into libros (nLibros, nTitulo, nAnnoPublicacion, nNumPaginas, nEditorial) values (?,?,?,?,?)";

		try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, libros.getnLibros());
			ps.setString(2, libros.getnTitulo());
			ps.setInt(3, libros.getnAnnoPublicacion());
			ps.setInt(4, libros.getnNumPaginas());
			ps.setString(5, libros.getnEditorial());

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al insertar el libros");
			//e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public boolean deleteLibros(int nLibros) {
		String sql = "delete from libros where nLibros = ?";

		try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, nLibros);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al borrar el libro");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public boolean updateLibros(int nLibros, String nTitulo, int nAnnoPublicacion, int nNumPaginas, String nEditorial) {
		String sql = "update libros set nLibros = ?, nTitulo = ?, nAnnoPublicacion= ?, nNumPaginas= ?, nEditorial= ? where nLibros = ?";

		try {
			PreparedStatement ps = con.prepareStatement(sql);

			
			ps.setInt(1, nLibros);
			ps.setString(2, nTitulo);
			ps.setInt(3, nAnnoPublicacion);
			ps.setInt(4, nNumPaginas);
			ps.setString(5, nEditorial);
			ps.setInt(6, nLibros);
			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al actualizar los libros");
			e.printStackTrace();
			return false;
		}

		return true;
	}

}
