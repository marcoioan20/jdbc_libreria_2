package escrito_por;

import java.sql.ResultSet;
import java.util.ArrayList;




public interface Escrito_porDAO {

	public ArrayList<Escrito_por> selectAllEscrito_por();

	public boolean insertLibros(Escrito_por escrito_por);

	public boolean deleteLibros(int nAutores);

	public boolean updateLibros(int nAutores, int nLibros);


	
}
