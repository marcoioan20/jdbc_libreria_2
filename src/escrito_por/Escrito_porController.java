package escrito_por;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.SQLException;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;

import libros.Libros;

public class Escrito_porController implements Escrito_porDAO {
	
	private Connection con;

	public Escrito_porController(Connection con) {
		super();
		this.con = con;
	}


	@Override
	public ArrayList<Escrito_por> selectAllEscrito_por(){
		
		ArrayList<Escrito_por> escrito = new ArrayList<Escrito_por>();

		try {
			String query = "Select * from escrito_por";
			Statement stm = con.createStatement();
			ResultSet rs = stm.executeQuery(query);

			// rs.beforeFirst();

			while (rs.next()) {
				int nAutores = rs.getInt("nAutores");
				int nLibros = rs.getInt("nLibros");
			

				Escrito_por escritop = new Escrito_por(nAutores, nLibros);
				escrito.add(escritop);

			}
			stm.close();
			rs.close();

		} catch (SQLException ex) {
			ex.printStackTrace();
		}

		return escrito;
	}

	@Override
	public boolean insertLibros(Escrito_por escrito_por) {
		String sql = "insert into escrito_por (nAutores, nLibros) values (?,?)";

		try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, escrito_por.getnAutores());
			ps.setInt(2, escrito_por.getnLibros());

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al insertar el escrito_por");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public boolean deleteLibros(int nAutores) {
		String sql = "delete from escrito_por where nAutores = ?";

		try {
			PreparedStatement ps = con.prepareStatement(sql);

			ps.setInt(1, nAutores);

			ps.executeUpdate();

		} catch (SQLException e) {
			System.out.println("Se ha producido un error al borrar en el escrito_por");
			e.printStackTrace();
			return false;
		}

		return true;
	}

	@Override
	public boolean updateLibros(int nAutores, int nLibros) {
		
			String sql = "update escrito_por set nAutores = ?, nLibros = ? where nAutores = ?";

			try {
				PreparedStatement ps = con.prepareStatement(sql);

			
				ps.setInt(1, nAutores);
				ps.setInt(2, nLibros);
				ps.setInt(3, nAutores);
				ps.executeUpdate();

			} catch (SQLException e) {
				System.out.println("Se ha producido un error al actualizar la tabla escrito_por");
				e.printStackTrace();
				return false;
			}

			return true;
	}


	




	






}
