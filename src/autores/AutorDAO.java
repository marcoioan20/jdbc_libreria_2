package autores;
 
import java.sql.ResultSet;
import java.util.ArrayList;


public interface AutorDAO {

	public ArrayList<Autor> selectAllAutores();

	public boolean insertAutor(Autor autor);

	public boolean deleteAutor(int nAutores);

	public boolean updateAutor(int nAutores, String dni, String nombre, String apellidos, int edad);



}
